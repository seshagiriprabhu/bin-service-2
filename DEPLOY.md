## How to deploy on vuln image

```bash
make
```
Files/directories to be removed after generating the binary - `server` 
1. `Makefile`
2. `.git/`
3. `OBJECTS/`
4. `server.c`
5. `.gitignore`
6. `DEPLOY.md`
7. `store_retreive.py`
8. `gameserver_script.py`

So only these files should remain the vuln image:
1. `server`
2. `README.md`
3. `setup_database.sh`
