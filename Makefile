CC = gcc
CFLAG = -c -g -Wall -W -fno-stack-protector
SQLITE = -l sqlite3
ODIR = OBJECTS
EXEC = server
OBJ = $(ODIR)/$(EXEC).o

all 	: $(EXEC)

$(ODIR) :
		  mkdir -p $@

$(OBJ) 	: | $(ODIR)

$(OBJ)	: $(EXEC).c 
		  $(CC) -o $@ $< $(CFLAG)

$(EXEC)	: $(OBJ)
		  $(CC) -o $@ $^ $(SQLITE)

.PHONY 	: clean all

clean 	:
		  rm -rf $(ODIR) $(EXEC)

run 	: 
		  ./server 
