# Secure Secular service
So the Congress BJP has come up with an election manifesto which touches the 
secular aspects of development, better infrastructure, economy, blah blah. But 
BJP is the only party which gave importance to Cyber Security 
[BJP manifesto](http://www.bjp.org/manifesto2014) (see page no 17 for proof). 
After looking at both parties' manifesto, we felt secure and hence we have 
decided to write a secure secular service. 

## Files given to the participants
1. `server`             - ELF-32, unstripped binary file 
2. `README.md`          - A quick info about the service and blah blah
3. `setup_database.sh`  - A shellscript to setup and create a SQLite DB to store
flags. A file named `service.db` will be created upon executing this script. 

## Requirements
This vulnerable program requires these packages: `sqlite3`, `libsqlite3-dev`

```bash
sudo apt-get install sqlite3 libsqlite3-dev
```
`aslr` should be disabled
```bash
echo 0 > /proc/sys/kernel/randomize_va_space
```

## How to setup the DB

```bash
./setup_database.sh
```

## How to run the server program

```bash
./server
```

## How to connect to the server program

You could use either `telnet` or `netcat` to connect to the server program. 
```bash
nc YOUR_VULN_IMAGE_IP 50001
```
or
```bash
telnet YOUR_VULN_IMAGE_IP 50001
```
Service port: `50001`

